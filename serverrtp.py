#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import random
import socketserver
import sys
import simplertp
import threading
import time
import socket
from threading import Timer

try:
    SERVERSIP = sys.argv[1]
    SIPIP = str(SERVERSIP.split(":")[0])
    SIPPORT = int(SERVERSIP.split(":")[1])
    AUDIO_FILE = sys.argv[2]
    NAMEUSER = sys.argv[2].split(".")[0]
    TIME = 3
except IndexError:
    print("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <file>")


class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """

    def handle(self):

        global sender
        readline = self.rfile.read()
        method = readline.decode('utf-8').split(" ")[0]
        if method == "INVITE":
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + str(readline) + ".")
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + "SIP/2.0 200 OK" + ".")
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        if method == "ACK":
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + str(readline) + ".")

            print(time.strftime('%Y%m%d%H%M%S', time.gmtime(
                time.time())) + " " + "RTP" + " " + "to" + " " + '127.0.0.1' + ":" + "4100" + ".")
            sender = simplertp.RTPSender(ip='127.0.0.1', port=4100, file='cancion.mp3', printout=True)
            sender.send_threaded()

        if method == "BYE":
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + str(readline) + ".")
            sender.finish()
            print("Se ha finalizado el envío")
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + "SIP/2.0 200 OK" + ".")

            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        if method != "INVITE" and method != "ACK" and method != "BYE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


def main():
    print(time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) + " " + "Starting...")
    with socketserver.UDPServer(('0.0.0.0', 5060), SIPHandler) as serv:
        REQUESTREGISTER = "REGISTER" + " " + "sip:" + NAMEUSER + "@singasong.net" + " " + "SIP/2.0" + "\r\n"
        print(time.strftime('%Y%m%d%H%M%S',
                            time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
            SIPPORT) + ":" + " " + REQUESTREGISTER.split("\r\n")[0] + ".")
        serv.socket.sendto(REQUESTREGISTER.encode('utf-8'), (SIPIP, SIPPORT))
        data = serv.socket.recv(2048)
        response = data.decode('utf-8').split('\r\n')[0]
        print(time.strftime('%Y%m%d%H%M%S',
                            time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
            SIPPORT) + ":" + " " + response)
        serv.serve_forever()


if __name__ == "__main__":
    main()
