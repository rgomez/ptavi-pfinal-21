#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys
import threading
import time
import socketserver
from threading import Timer

try:

    SERVERSIP = sys.argv[1]
    SIPIP = str(SERVERSIP.split(":")[0])
    SIPPORT = int(SERVERSIP.split(":")[1])
    DIRCLIENT = sys.argv[2].split(":")[1]
    USER = DIRCLIENT.split("@")[0]
    DIRSERVERRTP = str(sys.argv[3].split(":"))
    TIME = int(sys.argv[4])
    FILE = sys.argv[5]
    SDPDescription = f"\r\nv=0\r\no=sip:{DIRCLIENT} {SIPIP}\r\ns={USER}\r\nt=0\r\nm=audio {SIPPORT} RTP"
    cabecera1 = "Content - Type: application / sdp\r\n"
    length = len(SDPDescription)
    cabecera2 = f"Content-Length: {length}\r\n"
    SDPDescription = cabecera1 + cabecera2 + SDPDescription
except IndexError:
    print("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")


class RTPHandler(socketserver.BaseRequestHandler):
    def handle(self):
        msg = self.request[0]
        payload = msg[12:]
        self.FILE.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        cls.FILE = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.FILE.close()


def main():
    # Creamos el socket y lo configuramos
    print(time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) + " " + "Starting...")
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SIPIP, SIPPORT))
            REQUESTREGISTER = "REGISTER" + " " + "sip:" + DIRCLIENT + " " + "SIP/2.0" + "\r\n"
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + REQUESTREGISTER.split("\r\n")[0] + ".")
            my_socket.send(bytes(REQUESTREGISTER.encode('utf-8') + b'\r\n'))
            data = my_socket.recv(2048)
            response = data.decode('utf-8')
            response2 = response.split("\r\n\r\n")[0]
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + response2)
            if response2 == "SIP/2.0 200 OK":
                REQUESTINVITE = "INVITE" + " " + "sip:" + sys.argv[3].split(":")[1] + " " + "SIP/2.0" + "\r\n"
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                    SIPPORT) + ":" + " " + REQUESTINVITE.split("\r\n")[0] + ".")
                REQUESTINVITE += SDPDescription
                my_socket.send(bytes(REQUESTINVITE.encode('utf-8') + b'\r\n'))
                data2 = my_socket.recv(2048)
                response3 = data2.decode('utf-8')
                response4 = response3.split("\r\n\r\n")[0]
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                    SIPPORT) + ":" + " " + response4)
            if response4 == "SIP/2.0 200 OK":
                REQUESTACK = "ACK" + " " + "sip:" + sys.argv[3].split(":")[1] + " " + "SIP/2.0" + "\r\n"
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                    SIPPORT) + ":" + " " + REQUESTACK.split("\r\n")[0] + ".")

                my_socket.send(bytes(REQUESTACK.encode('utf-8') + b'\r\n'))
                data3 = my_socket.recv(2048)
                print(data3.decode('utf-8'))

        RTPHandler.open_output(FILE)
        with socketserver.UDPServer(("127.0.0.1", 4100), RTPHandler) as serv:
            rtp_port = serv.server_address[1]
            print(time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) + " " + "RTP" + " " + "ready" + " " + str(
                rtp_port) + ".")
            # El bucle de recepción (serv_forever) va en un hilo aparte,
            # para que se continue ejecutando este programa principal,
            # y podamos interrumpir ese bucle más adelante
            threading.Thread(target=serv.serve_forever).start()
            # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
            # por ejemplo
            time.sleep(TIME)
            print("Time passed, shutting down receiver loop.")
            serv.shutdown()
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as socket2:
            REQUESTBYE = "BYE" + " " + "sip:" + sys.argv[3].split(":")[1] + " " + "SIP/2.0" + "\r\n"
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + REQUESTBYE.split("\r\n")[0] + ".")
            socket2.sendto(REQUESTBYE.encode('utf-8'), (SIPIP, SIPPORT))
            data8 = socket2.recv(2048)
            response6 = data8.decode('utf-8')
            response7 = response6.split("\r\n\r\n")[0]
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + SIPIP + ":" + str(
                SIPPORT) + ":" + " " + response7)
            # Cerramos el fichero donde estamos escribiendo los datos recibidos
        RTPHandler.close_output()

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
