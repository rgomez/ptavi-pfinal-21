﻿## Parte básica
* Todo funciona bien.

## Parte adicional
* Cabecera de tamaño.
  * Aparece cuando el cliente envía el INVITE al servidor SIP y este lo reenvía al servidor RTP.

* Gestión de errores.
  * Se envían más respuestas en lugar de 200 OK cuando hay errores en el envío de paquetes.