#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import threading
import socket
import simplertp
import time

try:
    PORT = int(sys.argv[1])
    SERVER = 'localhost'
except IndexError:
    print("Usage: python3 serversip.py <port>")


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    registrar = {}

    def handle(self):
        readline = self.rfile.read()
        method = readline.decode('utf-8').split(" ")[0]
        name = readline.decode('utf-8').split(":")[1].split(" ")[0]
        IP = self.client_address[0]
        PORT = self.client_address[1]
        if method == "REGISTER":
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + IP + ":" + str(
                PORT) + ":" + " " + str(readline) + ".")
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            self.registrar[name] = (IP, PORT)

            with open('registrar.json', 'w') as jsonfichero:
                json.dump(self.registrar, jsonfichero, indent=2)

        if method == "INVITE":
            with open('registrar.json', 'r') as jsonfichero:
                self.registrar = json.load(jsonfichero)

            rtpserver = self.registrar.get('cancion@singasong.net')[0]
            portserver = 5060
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + IP + ":" + str(
                PORT) + ":" + " " + str(readline) + ".")
            self.socket.sendto(self.packet, (str(rtpserver), portserver))
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + rtpserver + ":" + str(
                portserver) + ":" + " " + str(readline) + ".")
            data = self.socket.recv(2048)
            response = data.decode('utf-8')
            response2 = response.split("\r\n")[0]
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + rtpserver + ":" + str(
                portserver) + ":" + " " + str(response2) + ".")

            if response2 == "SIP/2.0 200 OK":
                self.wfile.write(data)
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + IP + ":" + str(
                    PORT) + ":" + " " + str(response2) + ".")

        if method == "ACK":
            rtpserver = self.registrar.get('cancion@singasong.net')[0]
            portserver = 5060
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + IP + ":" + str(
                PORT) + ":" + " " + str(readline) + ".")
            self.socket.sendto(self.packet, (str(rtpserver), portserver))
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + rtpserver + ":" + str(
                portserver) + ":" + " " + str(readline) + ".")
            data2 = self.socket.recv(2048)

        if method == "BYE":

            rtpserver = self.registrar.get('cancion@singasong.net')[0]
            portserver = 5060
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "from" + " " + IP + ":" + str(
                PORT) + ":" + " " + str(readline) + ".")
            self.socket.sendto(self.packet, (str(rtpserver), portserver))
            print(time.strftime('%Y%m%d%H%M%S',
                                time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + rtpserver + ":" + str(
                portserver) + ":" + " " + str(readline) + ".")
            data3 = self.socket.recv(2048)
            response3 = data3.decode('utf-8')
            response4 = response3.split("\r\n")[0]
            if response4 == "SIP/2.0 200 OK":
                self.wfile.write(data3)
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(
                                        time.time())) + " " + "SIP" + " " + "from" + " " + rtpserver + ":" + str(
                    portserver) + ":" + " " + str(response4) + ".")
            if response4 == "SIP/2.0 200 OK":
                self.wfile.write(data3)
                print(time.strftime('%Y%m%d%H%M%S',
                                    time.gmtime(time.time())) + " " + "SIP" + " " + "to" + " " + IP + ":" + str(
                    PORT) + ":" + " " + str(response4) + ".")

        if method != "INVITE" and method != "ACK" and method != "BYE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


def main():
    print(time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time())) + " " + "Starting...")
    # Creamos servidor de eco y escuchamos
    with socketserver.UDPServer(('', PORT), EchoHandler) as serv:
        serv.serve_forever()


if __name__ == "__main__":
    main()
